<?php

use app\components\Router;


require_once(__DIR__ . '/vendor/autoload.php');

define("ROOT", __DIR__ . '/app');
define("CONTROLLERS", "Controllers\\");


$router = new Router();
$router->run();
