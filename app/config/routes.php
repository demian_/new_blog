<?php
return [
    "posts\/view\?post=([0-9]+)" => [
        "controller" => "post",
        "action" => "view"
    ],
    "posts\?page=([0-9]+)$" => [
        "controller" => "post",
        "action" => "index"
    ],
    "posts\/create" => [
        "controller" => "post",
        "action" => "create"
    ],
    "posts\/public" => [
        "controller" => "post",
        "action" => "public"
    ],
    "posts\/update" => [
        "controller" => "post",
        "action" => "update"
    ],
    "posts\/edit\?post=([0-9]+)" => [
        "controller" => "post",
        "action" => "edit"
    ],
    "posts\/remove\?post=([0-9]+)" => [
        "controller" => "post",
        "action" => "remove"
    ],
    "login" => [
        "controller" => "user",
        "action" => "loginForm"
    ],
    "user\/login" => [
        "controller" => "user",
        "action" => "login"
    ],
    "register" => [
        "controller" => "user",
        "action" => "registerForm"
    ],
    "user\/register" => [
        "controller" => "user",
        "action" => "register"
    ],
    "^\/$" => [
        "controller" => "main",
        "action" => "index"
    ],
];