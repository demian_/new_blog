<?php


namespace app\models;


use app\components\DBConnector;
use app\components\PassEncrypter;

class User
{
    private $dbConnector;

    public function __construct()
    {
        $this->dbConnector = new DBConnector();
    }

    public function createUser($login, $pass)
    {
        if ($this->isRegister($login)) {
            $encryptedPass = PassEncrypter::encrypt($pass);
            $query = "INSERT INTO users (login, password) VALUES (?, ?)";
            $this->dbConnector->exec($query, $login, $encryptedPass);
            $this->initializeSession($login, $encryptedPass);
        }
    }

    public function login($login, $pass)
    {
        $encryptedPass = PassEncrypter::encrypt($pass);
        if ($this->checkUser($login, $encryptedPass)) {
            $this->initializeSession($login, $encryptedPass);
        }
    }

    private function isRegister($login)
    {
        $query = "SELECT * FROM users WHERE login = ?";
        $answer = $this->dbConnector->query($query, $login);
        $result = array_shift($answer);
        return empty($result);
    }


    private function checkUser($login, $pass)
    {
        $query = "SELECT * FROM users WHERE login = ? AND password = ?";
        $answer = $this->dbConnector->query($query, $login, $pass);
        $result = array_shift($answer);
        return !empty($result);
    }

    private function initializeSession($login, $pass)
    {
        $_SESSION["login"] = $login;
        $_SESSION["pass"] = $pass;
    }
}