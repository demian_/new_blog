<?php

namespace app\models;

use app\components\DBConnector;

class Post
{
    private $dbConnector;
    public function __construct()
    {
        $this->dbConnector = new DBConnector();
    }

    public function getPosts($from, $to)
    {
        $query = "SELECT * FROM posts ORDER BY id DESC LIMIT ? , ?";
        return $this->dbConnector->query($query, $from, $to);
    }

    public function getPostById($id)
    {
        $query = "SELECT * FROM posts WHERE id = ?";
        return $this->dbConnector->query($query, $id);
    }

    public function createPost()
    {
        $title = $_POST["title"];
        $content = $_POST["content"];
        $query = "INSERT INTO posts (title, date, content) VALUES (?, NOW(), ?)";
        $this->dbConnector->exec($query, $title, $content);
    }

    public function editPost($id, $title, $content)
    {
        $query = "UPDATE posts SET title = ?, content = ?, date = NOW() WHERE id = ?";
        $this->dbConnector->exec($query, $title, $content, $id);
    }

    public function removePost($id)
    {
        $query = "DELETE FROM posts WHERE id = ?";
        $this->dbConnector->exec($query, $id);
    }

    public function getPostsCount()
    {
        $query = "SELECT COUNT(*) FROM posts AS count";
        $result = $this->dbConnector->query($query);
        return $result;
    }
}
