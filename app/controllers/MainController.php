<?php


namespace app\controllers;


use app\views\MainView;

class MainController
{
    private $view;
    public function __construct()
    {
        $this->view = new MainView();
    }

    public function actionIndex()
    {
        $this->view->renderIndex();
    }

}
