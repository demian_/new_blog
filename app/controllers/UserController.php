<?php


namespace app\controllers;


use app\models\User;
use app\views\UserView;

class UserController
{
    private $model;
    private $view;
    public function __construct()
    {
        $this->model = new User();
        $this->view = new UserView();
    }

    public function actionLoginForm()
    {
        $this->view->renderLoginForm();
    }

    public function actionLogin()
    {
        $this->model->login($_POST["login"], $_POST["pass"]);
        header("Location: http://localhost");
    }

    public function actionRegisterForm()
    {
        $this->view->renderRegisterForm();
    }


    public function actionRegister()
    {
        $this->model->createUser($_POST["login"], $_POST["pass"]);
        header("Location: http://localhost");
    }
}