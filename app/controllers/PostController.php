<?php

namespace app\controllers;

use app\components\Pager;
use app\models\Post;
use app\views\PostView;

class PostController
{
    private $model;
    private $view;
    private $pager;

    public function __construct()
    {
        $this->model = new Post();
        $this->view = new PostView();
        $this->pager = new Pager();
    }

    public function actionEdit($post)
    {
        $posts = $this->model->getPostById(...array_values($post));
        $post = array_shift($posts);
        $this->view->renderEditForm($post);
    }

    public function actionView($args)
    {
        $post = $this->model->getPostById(array_shift($args));
        $this->view->renderPost(...$post);
    }

    public function actionCreate()
    {
        $this->view->renderCreation();
    }

    public function actionPublic($args)
    {
        $this->model->createPost();
        header("Location: http://localhost/posts?page=1");
    }

    public function actionIndex($args)
    {
        $currentPage = 1;
        $this->pager->countPages(...$this->model->getPostsCount());
        $from = $this->pager->getInterval(...array_values($args));
        $posts = $this->model->getPosts($from, Pager::POSTS_ON_PAGE);
        $this->view->renderPosts($posts, $this->pager->getCurrentPage(), $this->pager->getPages());
    }

    public function actionRemove($args)
    {
        $this->model->removePost($args["post"]);
        header("Location: http://localhost/posts?page=1");
    }

    public function actionUpdate()
    {
        $this->model->editPost($_POST["id"], $_POST["title"], $_POST["content"]);
        header("Location: http://localhost/posts?page=1");

    }
}
