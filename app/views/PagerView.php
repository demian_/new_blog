<?php


namespace app\views;


class PagerView
{
    public static function renderPagination($currentPage, $pages): string
    {
        $result = "<div id='paging'>";
        if ($currentPage > 1) {
            $result .= "<a href='http://localhost/posts?page=1'><<<-</a>";
            $result .= "<a href='http://localhost/posts?page=" . ($currentPage - 1) . "'> <- </a>";
        }
        $result .= "<b>$currentPage</b>";
        if ($currentPage != $pages) {
            $result .= "<a href='http://localhost/posts?page=" . ($currentPage + 1) . "'> -> </a>";
            $result .= "<a href='http://localhost/posts?page=" . $pages . "'> ->>> </a>";
        }
        $result .= "</div>";
        return $result;

    }
}