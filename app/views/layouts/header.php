<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog</title>
    <style>
        body {
            background: mistyrose;
        }

        .container {
            font-family: "Lucida Console", Monaco, monospace;
            text-align: center;
        }

        form {
            display: inline-block;
            line-height: 1.5;
            padding-top: 200px;
            padding-bottom: 200px;
        }


        #main {
            line-height: 1.5;
            padding-top: 200px;
            padding-bottom: 200px;
        }

        #paging {
            font-family: "Lucida Console", Monaco, monospace;
            text-align: center;
        }

        a:hover {
            text-decoration: underline;
        }

        a {
            color: hotpink;
            text-decoration: none;
        }

    </style>
</head>
<body>