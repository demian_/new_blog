<div class="container" id="creatingForm">
    <form action="http://localhost/posts/public" method="post">
        <table>
            <tr>
                <td>Title</td>
                <td><input type="text" name="title" autocomplete="off" required minlength="5" maxlength="30"></td>
            </tr>
            <tr>
                <td>Content</td>
                <td><textarea style="resize: none" name="content" cols="20" rows="10" required minlength="5" maxlength="30"></textarea></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Submit"></td>
            </tr>
        </table>
    </form>
</div>