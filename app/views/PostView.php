<?php


namespace app\views;


class PostView
{
    public function renderPosts($posts, $currentPage, $pages)
    {
        require_once ROOT . "/views/layouts/header.php";
        echo "<div class='container'><h1>Posts</h1>";
        echo "<p><a href=\"http://localhost/posts/create\">Create new post</a></p></div>";
        foreach ($posts as $post) {
            echo "<div class='container'>";
            echo "<p><b><a href=\"http://localhost/posts/view?post={$post['id']}\">{$post['title']}</a> </b>";
            echo "(" . date("D j M Y - G:i:s", strtotime($post['date'])) . ")</p>";
            echo "<p><i>{$post['content']}</i></p>";
            echo "</div>";
            echo "<hr>";
        }
        echo PagerView::renderPagination($currentPage, $pages);
        require_once ROOT . "/views/layouts/footer.php";
    }

    public function renderPost($post)
    {
        require_once ROOT . "/views/layouts/header.php";
        echo "<div class='container'><h1>Post</h1>";
        echo "<p><b>{$post['title']} </b>";
        echo "(" . date("D j M Y - G:i:s", strtotime($post['date'])) . ")</p>";
        echo "<p><i>{$post['content']}</i></p>";
        echo "<p><a href='http://localhost/posts/edit?post={$post['id']}'>Edit post</a></p>";
        echo "<p><a href='http://localhost/posts/remove?post={$post['id']}'>Remove post</a></p>";
        echo "</div>";
        echo "<hr>";
        require_once ROOT . "/views/layouts/footer.php";
    }

    public function renderCreation()
    {
        require_once ROOT . "/views/layouts/header.php";
        require_once ROOT . "/views/layouts/form.php";
        require_once ROOT . "/views/layouts/footer.php";
    }

    public function renderEditForm($post)
    {
        require_once ROOT . "/views/layouts/header.php";
        echo "<div class=\"container\" id=\"form\">
    <form action=\"http://localhost/posts/update\" method=\"post\">
    <input type='hidden' name='id' value='{$post['id']}'> 
        <table>
            <tr>
                <td>Title</td>
                <td><input type=\"text\" name=\"title\" required minlength=\"5\" maxlength=\"30\" autocomplete=\"off\" value=\"{$post['title']}\"></td>
            </tr>
            <tr>
                <td>Content</td>
                <td><textarea style=\"resize: none\" name=\"content\" required minlength=\"5\" maxlength=\"30\" cols=\"20\" rows=\"10\">{$post['content']}</textarea></td>
            </tr>
            <tr>
                <td colspan=\"2\"><input type=\"submit\" value=\"Submit\"></td>
            </tr>
        </table>
    </form>
</div>";
        require_once ROOT . "/views/layouts/footer.php";
    }
}
