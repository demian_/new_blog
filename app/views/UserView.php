<?php


namespace app\views;


class UserView
{
    public function renderRegisterForm()
    {
        require_once ROOT . "/views/layouts/header.php";
        echo "
        <div class=\"container\">
        <h2>Registration</h2>
        <form action=\"http://localhost/user/register\" method=\"post\">
        <p><input type=\"text\" name=\"login\" placeholder=\"Enter your login\" autocomplete=\"off\" required minlength=\"3\" maxlength=\"15\"></p>
        <p><input type=\"text\" name=\"pass\" placeholder=\"Enter your password\" autocomplete=\"off\" required minlength=\"3\" maxlength=\"15\"></p>
        <p><input type=\"submit\" value=\"Submit\"></p>
        </form>
        </div>
        ";
        require_once ROOT . "/views/layouts/footer.php";
    }

    public function renderLoginForm()
    {
        require_once ROOT . "/views/layouts/header.php";
        echo "
        <div class=\"container\">
        <h2>Log in</h2>
        <form action=\"http://localhost/user/login\" method=\"post\">
        <p><input class='input' type=\"text\" name=\"login\" placeholder=\"Enter your login\" autocomplete=\"off\" required minlength=\"5\" maxlength=\"30\"></p>
        <p><input type=\"password\" name=\"pass\" placeholder=\"Enter your password\" required minlength=\"5\" maxlength=\"30\"></p>
        <p><input type=\"submit\" value=\"Submit\"></p>
        </form>
        </div>
        ";
        require_once ROOT . "/views/layouts/footer.php";
    }
}