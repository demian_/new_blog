<?php

namespace app\components;


//use Throwable;

class Router
{
    private $uri;
    private $routes;
    private $query;
    private $path;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->routes = include(ROOT . "/config/routes.php");
        parse_str($_SERVER["QUERY_STRING"], $this->query);
        $this->parseQuery($this->query);
        $this->uri = $this->parseUri($_SERVER["REQUEST_URI"]);
        $this->path = explode("/", $this->uri);
    }

    private function parseUri(string $uri): string
    {
        $uri = trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
        return $uri;
    }

    private function parseQuery(&$query)
    {
        foreach ($query as &$item) {
            $item = htmlspecialchars(trim($item));
        }
    }

    public function run()
    {
        foreach ($this->routes as $route => $elements) {
            if (preg_match("#$route#", $_SERVER["REQUEST_URI"])) {

                [$controllerReduction, $actionName] = array_values($elements);
                $controllerClassNameWithNamespace = 'app\controllers\\' . ucfirst($controllerReduction) . 'Controller';
                $controllerClassName = ucfirst($controllerReduction) . 'Controller';

                try {
                    require_once(ROOT . "/controllers/$controllerClassName.php");
                    session_start();
                    $controller = new $controllerClassNameWithNamespace();
                    $controller->{'action' . $actionName}($this->query);
                } catch (Throwable $e) {
                    print_r($e);
                }
            }
        }
    }
}
