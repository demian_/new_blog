<?php


namespace app\components;


class Pager
{
    public const POSTS_ON_PAGE = 5;
    private $pages = 1;
    private $currentPage = 1;


    public function countPages($args)
    {
        $posts = array_shift($args);
        $this->pages = ceil($posts / self::POSTS_ON_PAGE);
    }

    public function getInterval($page): int
    {
        if ($page < 1) {
            $from = 0;
        } elseif ($page > $this->pages) {
            $from = ($this->pages - 1) * self::POSTS_ON_PAGE;
            $this->currentPage = $this->pages;
        } else {
            $from = ($page - 1) * self::POSTS_ON_PAGE;
            $this->currentPage = $page;
        }
        return $from;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

}