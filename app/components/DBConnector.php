<?php


namespace app\components;


class DBConnector
{
    private $params;
    private $pdo;
    private $pdoStatement;
    private $validator;

    public function __construct()
    {
        $this->params = include ROOT . "/config/db.php";
        $this->pdo = new \PDO("mysql:host={$this->params['host']};dbname={$this->params['dbname']}"
            , $this->params["user"], $this->params["pass"]);
        $this->pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, FALSE);
        $this->validator = new QueryValidator($this->pdo);
    }

    public function query($query, ...$args)
    {
        $this->pdoStatement = $this->validator->validateQuery($query, ...$args);
        $result = $this->pdoStatement->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function exec($query, ...$args)
    {
        $this->validator->validateQuery($query, ...$args);
    }
}
