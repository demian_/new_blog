<?php


namespace app\components;


class QueryValidator
{
    /**
     * @var \PDO
     */
    public $pdo;

    public function __construct(&$pdo)
    {
        $this->pdo = $pdo;
    }

    public function validateQuery($query, ...$args): \PDOStatement
    {
        $pdoResult = $this->pdo->prepare($query);
        $pdoResult->execute($args);
        return $pdoResult;
    }
}